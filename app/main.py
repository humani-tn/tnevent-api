import os
import re
from datetime import datetime
import urllib.request
import urllib.error
import urllib.parse
from typing import Dict, List, Union, cast
from json import JSONEncoder
from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from bs4 import BeautifulSoup
import yaml
from dotenv import load_dotenv
from yaml.loader import SafeLoader

from app.twitchaccess import get_stream_info
from app.api_utils import ConnectionManager, ThreadJob, ThreadJobAsync
from app.datatypes import (
    ParamType,
    SponsorIndexed,
    StreamerData,
    TeamMemberIndexed,
    BaseModel,
)
from app.statistics import register_stats
from app.caching import cache

load_dotenv()
PARAMFILE = os.getenv("API_CONFIG", default="config.yaml")

socket_manager = ConnectionManager()

##### Utility functions #####


def load_params():
    """
    Loading all the parameters from the config file and get the informations from twitch api
    about streamers
    """
    with open(PARAMFILE, "r") as file:
        params = yaml.load(file, Loader=SafeLoader)
        cache.set("params", params)


def get_streamers_live_infos():
    params = cache.get("params")
    if params is None:
        load_params()
        params = cache.get("params")
    info_stream = get_stream_info(
        [streamer["twitchId"] for streamer in params["streams"]]
    )
    # loading streamers infos
    streams: List[StreamerData] = []
    offline_streamers: List[StreamerData] = []
    totalviews = 0
    for streamer in params["streams"]:
        info_streamer = info_stream[streamer["twitchId"]]
        if info_streamer["isLive"]:
            streams.insert(0, {**streamer, **info_streamer})
        else:
            offline_streamers.append({**streamer, **info_streamer})
        totalviews += info_streamer["viewerCount"]
    cache.set("totalViewers", totalviews)
    streamers_data = [*streams, *offline_streamers]
    cache.set("streamers_data", streamers_data)


async def update_stats_websocket():
    """
    Broadcasting statistics to all connected websockets
    """
    await socket_manager.broadcast(
        JSONEncoder().encode(
            {
                "donation_amount": cache.get("donation_amount"),
                "totalViewers": cache.get("totalViewers"),
                "visitors": socket_manager.active_connection_number(),
            }
        )
    )


async def update_donations():
    """
    Get the amount of donations from an Action Enfance pot by parsing the web page
    """
    params = cache.get("params")
    if params is None:
        load_params()
        params = cache.get("params")
    url = params["donationlink"] + "/widget"
    try:
        response = urllib.request.urlopen(url)
        web_content = response.read().decode("UTF-8")
        # Parsing the web page
        soup = BeautifulSoup(web_content, features="lxml")
        txt = soup.find_all(class_="info-money")[0].text.strip()[:-2]
        clean_txt = re.sub(r"[^0-9\.,]", "", txt).replace(",", ".")
        cache.set("donation_amount", float(clean_txt))
    except Exception as exep:
        print(f"<DonationParser> Exception while parsing {exep}")


async def register_metrics():
    """
    Registers some metrics from the event in the database
    """
    params = cache.get("params")
    if params is None:
        load_params()
        params = cache.get("params")
    donation_amount: float = cache.get("donation_amount")
    streamers_data: List[StreamerData] = cache.get("streamers_data")

    if datetime.now() >= datetime.strptime(
        params["date"], "%Y-%m-%d %H:%M:%S"
    ) and datetime.now() <= datetime.strptime(params["endDate"], "%Y-%m-%d %H:%M:%S"):
        # If the event is started
        await register_stats(
            streamers_data,
            donation_amount,
            0,
            socket_manager.active_connection_number(),
        )
        print("<StatisticsManager> Statistics registered")


##### App init #####

load_params()

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")


origins: List[str] = cast(ParamType, cache.get("params"))["origins"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup_event():
    """
    Running some job for fetching data in separate threads
    """

    async def data_reloader():
        """
        Update the donations and send the data to the connected websockets
        """
        await update_donations()
        await update_stats_websocket()

    def params_reloader():
        """
        Update the parameters and streamers infos
        """
        load_params()
        get_streamers_live_infos()

    await update_donations()
    get_streamers_live_infos()
    donations_reload = ThreadJobAsync(data_reloader, 20)
    donations_reload.start()
    params_reload = ThreadJob(params_reloader, 20)
    params_reload.start()
    register_metrics_reload = ThreadJobAsync(register_metrics, 60)
    register_metrics_reload.start()
    cache.set("params_reload", params_reload)
    cache.set("donations_reload", donations_reload)
    cache.set("register_metrics_reload", register_metrics_reload)


@app.on_event("shutdown")
def shutdown_event():
    """
    Stop all the tasks
    """
    params_reload: ThreadJob = cache.get("params_reload")
    donations_reload: ThreadJobAsync = cache.get("donations_reload")
    register_metrics_reload: ThreadJobAsync = cache.get("register_metrics_reload")
    params_reload.stop()
    donations_reload.stop()
    register_metrics_reload.stop()


##### Routes #####


@app.get("/")
async def root():
    """
    Return the event name
    """
    params = cache.get("params")
    return {"appname": params["appname"]}


@app.get(
    "/streamers",
    response_model=List[StreamerData],
    response_description="A list of streamers",
)
async def streamers():
    """
    Get all the streamers of the event
    """
    streamers_data = cache.get("streamers_data")
    return streamers_data


@app.get("/donationlink", response_model=Dict[str, str])
async def donationlink():
    """
    The link to the donation page
    """
    params = cache.get("params")
    return {"link": params["donationlink"]}


@app.get("/donations", response_model=Dict[str, int])
async def donations():
    """
    The actual amount of donations
    """
    donation_amount = cache.get("donation_amount")
    return {"amount": donation_amount}


@app.get("/sponsors", response_model=List[SponsorIndexed])
async def sponsors():
    params = cache.get("params")
    i = 0

    def addidx(item):
        nonlocal i
        item["id"] = i
        i += 1
        return item

    return list(map(addidx, params["sponsors"]))


@app.get("/team", response_model=List[TeamMemberIndexed])
async def team():
    """
    Get all the team members
    """
    params = cache.get("params")
    i = 0

    def addidx(elem):
        nonlocal i
        elem["id"] = i
        i += 1
        return elem

    return list(map(addidx, params["team"]))


@app.get("/team/{index}", response_model=Union[TeamMemberIndexed, BaseModel])
async def team_member(index: int):
    """
    Informations about a team member
    """
    params = cast(ParamType, cache.get("params"))
    try:
        return params["team"][index]
    except BaseException:
        return {}


@app.get("/startdate")
async def start_date():
    """
    The starting adn closing dates of the event
    """
    params = cache.get("params")
    return {"date": params["date"], "endDate": params["endDate"]}


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await socket_manager.connect(websocket)
    print("<SocketEvent> Somebody connected")
    try:
        while True:
            await websocket.receive_text()
    except WebSocketDisconnect:
        socket_manager.disconnect(websocket)
        print("<SocketEvent> Somebody disconnected")
