from typing import Union
import os
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase
from dotenv import load_dotenv

load_dotenv()

db: Union[AsyncIOMotorDatabase, None] = None


def get_mongo_connection() -> Union[AsyncIOMotorDatabase, None]:
    global db
    if db is not None:
        return db
    mongo_connection = os.getenv("MONGO_DB_ACCESS", default=None)
    if mongo_connection is None:
        print("<DBAccess> No db")
        return None
    try:
        client = AsyncIOMotorClient(mongo_connection)
        db = client.event
        return db
    except BaseException:
        print("<DBAccess> Error while connecting to the database")
        return None
