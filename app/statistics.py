"""
Provide the functions to register the statistics of the event
"""
from datetime import datetime
from typing import List, TypedDict
from bson import ObjectId
from app.datatypes import StreamerData
from app.dbaccess import get_mongo_connection

db = get_mongo_connection()


class StreamerStat(TypedDict):
    """
    A object to store the actual statistics of a streamer
    """

    name: str
    online: bool
    viewers: int


class Stats(TypedDict):
    """
    A mongo document storing the actual statistics of the event
    """

    id: ObjectId
    donation_amount: float
    raffle_tickets: int
    time: datetime
    connected_users: int
    streamers: List[StreamerStat]


def get_streamer_stats(streamer: StreamerData) -> StreamerStat:
    """
    Get informations from a StreamerData object and put them into a StreamerStat object
    """
    return StreamerStat(
        name=streamer.name, online=streamer.isLive, viewers=streamer.viewerCount
    )


async def register_stats(
    streamers: List[StreamerData],
    donation_amount: float,
    raffle_tickets: int,
    connected_users: int,
):
    """
    Register the current statistics of the event in the database
    """
    if db is None:
        return
    streamers_stats = []
    for streamer in streamers:
        streamers_stats.append(get_streamer_stats(streamer))
    stats = Stats(
        donation_amount=donation_amount,
        time=datetime.now(),
        raffle_tickets=raffle_tickets,
        streamers=streamers_stats,
        connected_users=connected_users,
    )
    await db.statistics.insert_one(stats)
