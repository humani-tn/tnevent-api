"""
Provide a caching system to store the data shared between the functions of the application
"""
from datetime import datetime
from typing import Dict, TypedDict, Union


class CacheElement(TypedDict):
    """
    An element of an APICache instance
    """

    value: object
    lastUpdate: datetime


class APICache:
    """
    A dictionnary based caching system
    """

    def __init__(self):
        # Dictionnary containing the data
        self.data: Dict[str, CacheElement] = {}

    def set(self, key: str, value: object):
        """
        Set the value of a key
        """
        self.data[key] = CacheElement(value=value, lastUpdate=datetime.now())

    def get(self, key: str) -> Union[object, None]:
        """
        Get the value of a key
        """
        try:
            return self.data[key]["value"]
        except KeyError:
            return None

    def get_last_update(self, key: str) -> Union[datetime, None]:
        """
        Get the time of the last update of a value
        """
        try:
            return self.data[key]["lastUpdate"]
        except KeyError:
            return None


cache = APICache()
