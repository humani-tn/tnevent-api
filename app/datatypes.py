"""
Provide the types used in the application
"""
from typing import List, TypedDict
from pydantic import BaseModel


class Stream(BaseModel):
    """
    Informations about a stream
    """

    name: str
    description: str
    twitchId: str
    img: str


class StreamerData(Stream):
    """
    Informations about a stream with current statistics
    """

    isLive: bool
    viewerCount: int


class TeamMember(BaseModel):
    """
    Informations about an event team member
    """

    name: str
    description: str
    role: str
    img: str


class TeamMemberIndexed(TeamMember):
    """
    Informations about an event team member with an index
    """

    id: int


class Sponsor(BaseModel):
    """
    Informations about an event sponsor
    """

    name: str
    description: str
    link: str
    img: str


class SponsorIndexed(Sponsor):
    """
    Informations about an event sponsor with an index
    """

    id: int


class ParamType(TypedDict):
    """
    Structure of a config file
    """

    appname: str
    origins: List[str]
    date: str
    endDate: str
    donationlink: str
    raffle_ticketsLimit: int
    streams: List[Stream]
    team: List[TeamMember]
    sponsors: List[Sponsor]
