"""
Provide the access to the Twitch API
"""
import os
from typing import Dict, List
from dotenv import load_dotenv
from twitchAPI.twitch import Twitch

load_dotenv()
TWITCH_APPID = os.getenv("API_TWITCH_APPID")
TWITCH_TOKEN = os.getenv("API_TWITCH_TOKEN")


twitch = Twitch(TWITCH_APPID, TWITCH_TOKEN)


def get_stream_info(streamers_login: List[str]) -> Dict[str, Dict[str, object]]:
    """
    Get the live informations about streamers.

    Actually limited to 100 streamers per requests.
    """
    streams_list = {}
    data = twitch.get_streams(user_login=streamers_login, first=100)
    for stream in data["data"]:
        streams_list[stream["user_login"]] = {
            "isLive": True,
            "viewerCount": stream["viewer_count"],
        }
    for streamer in streamers_login:
        if streamer not in streams_list:
            streams_list[streamer] = {"isLive": False, "viewerCount": 0}
    return streams_list
