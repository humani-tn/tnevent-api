"""
Provide some useful classes to build an API
"""
import threading
import asyncio
from typing import Callable, List
from fastapi import WebSocket


class ThreadJob(threading.Thread):
    """
    Create a thread to run a task periodically
    """

    def __init__(self, callback: Callable, interval: float):
        """Runs the callback function after `interval` seconds

        :param callback:  callback function to invoke
        :param interval: time in seconds after which are required to fire the callback
        """
        self.callback = callback
        self.event = threading.Event()
        self.interval = interval
        super().__init__()

    def run(self):
        """
        Start the thread and launch `callback` every `interval` seconds
        """
        while not self.event.wait(self.interval):
            self.callback()

    def stop(self):
        """
        Stop the thread
        """
        self.event.set()


class ThreadJobAsync(threading.Thread):
    """
    Create a thread to run an async task periodically
    """

    def __init__(self, callback, interval):
        """runs an async callback function after interval seconds

        :param callback: an async callback function to invoke
        :param interval: time in seconds after which are required to fire the callback
        """
        self.callback = callback
        self.event = threading.Event()
        self.interval = interval
        self.loop = asyncio.get_running_loop()
        super().__init__()

    def run(self):
        """
        Start the thread and launch `callback` asynchronously every `interval` seconds
        """
        while not self.event.wait(self.interval):
            self.loop.create_task(self.callback())

    def stop(self):
        """
        Stop the thread
        """
        self.event.set()


class ConnectionManager:
    """
    Manage WebSocket connections
    """

    def __init__(self):
        """
        Initialize the object with an empty list of websockets
        """
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        """
        Connect a new websocket and register it.

        :param websocket: a new websocket
        """
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        """
        Remove a disconnected websocket

        :param websocket: the websoket to remove
        """
        self.active_connections.remove(websocket)

    def active_connection_number(self) -> int:
        """
        Get the actual number of connected websockets
        """
        return len(self.active_connections)

    async def send_message(self, message: str, websocket: WebSocket):
        """
        Send a message to a given WebSocket

        :param message: the message to send
        :param websocket: the recipient websocket
        """
        await websocket.send_text(message)

    async def broadcast(self, message: str):
        """
        Broadcast a message to all WebSockets

        :param message: the message to send
        """
        for connection in self.active_connections:
            await connection.send_text(message)
