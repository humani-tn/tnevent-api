set -e

mongo <<EOF
use event
db.createUser({
    user: '$MONGO_EVENT_USER',
    pwd: '$MONGO_EVENT_PASSWORD',
    roles: [
        {
            role: 'readWrite',
            db: 'event',
        },
    ],
});

db.createCollection('statistics', { capped: false });
EOF